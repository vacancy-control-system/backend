package by.klishevich.vacancy_control_system.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import by.klishevich.vacancy_control_system.criteria.ChatCriteria;
import by.klishevich.vacancy_control_system.dto.chat.ChatDto;
import by.klishevich.vacancy_control_system.dto.chat.MessageDto;
import by.klishevich.vacancy_control_system.entity.PageDto;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.service.ChatService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/chats")
public class ChatController {
    private final ChatService service;

    @GetMapping(params = { "pageNumber", "pageSize" })
    public ResponseEntity<PageDto<ChatDto>> findAllPageable(
            ChatCriteria criteria) {
        return new ResponseEntity<>(service.findAllPageable(criteria), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<ChatDto>> findAll(
            ChatCriteria criteria) {
        return new ResponseEntity<>(service.findAll(criteria), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ChatDto> findById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/messages")
    public ResponseEntity<List<MessageDto>> findMessages(@PathVariable Long id, @RequestAttribute UserEntity user) {
        return new ResponseEntity<>(service.findMessages(id, user), HttpStatus.OK);
    }

    @GetMapping(value = "/my", params = { "pageNumber", "pageSize" })
    public ResponseEntity<PageDto<ChatDto>> findMyPageable(
            ChatCriteria criteria,
            UserEntity user) {
        return new ResponseEntity<>(service.findAllPageable(criteria, user), HttpStatus.OK);
    }

    @GetMapping(value = "/my")
    public ResponseEntity<List<ChatDto>> findMy(
            ChatCriteria criteria,
            UserEntity user) {
        return new ResponseEntity<>(service.findAll(criteria, user), HttpStatus.OK);
    }

    @GetMapping(value = "/my/{id}")
    public ResponseEntity<ChatDto> findMyById(
            @PathVariable Long id,
            UserEntity user) {
        return new ResponseEntity<>(service.findById(id, user), HttpStatus.OK);
    }
}
