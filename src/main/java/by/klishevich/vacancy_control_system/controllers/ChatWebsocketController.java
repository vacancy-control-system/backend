package by.klishevich.vacancy_control_system.controllers;

import java.security.Principal;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import by.klishevich.vacancy_control_system.security.CustomAuthentication;
import by.klishevich.vacancy_control_system.service.ChatService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ChatWebsocketController {
    private final ChatService service;

    @MessageMapping("chat/{id}/send-message")
    public void greeting(@DestinationVariable Long id, String message, Principal principal) throws Exception {
        service.createMessage(id, ((CustomAuthentication)principal).getUser(), message);
    }
}
