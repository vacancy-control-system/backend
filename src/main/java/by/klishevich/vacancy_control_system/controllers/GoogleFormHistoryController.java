package by.klishevich.vacancy_control_system.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import by.klishevich.vacancy_control_system.criteria.GoogleFormHistoryCriteria;
import by.klishevich.vacancy_control_system.dto.google_form_history.GoogleFormHistoryCreateRequest;
import by.klishevich.vacancy_control_system.dto.google_form_history.GoogleFormHistoryDto;
import by.klishevich.vacancy_control_system.entity.PageDto;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.service.GoogleFormHistoryService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/google-form-history")
public class GoogleFormHistoryController {
    private final GoogleFormHistoryService service;

    @GetMapping(params = { "pageNumber", "pageSize" })
    public ResponseEntity<PageDto<GoogleFormHistoryDto>> findAllPageable(
        GoogleFormHistoryCriteria criteria) {
        return new ResponseEntity<>(service.findAllPageable(criteria), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<GoogleFormHistoryDto>> findAll(
        GoogleFormHistoryCriteria criteria) {
        return new ResponseEntity<>(service.findAll(criteria), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<GoogleFormHistoryDto> findById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<GoogleFormHistoryDto> create(
            @Valid @RequestBody GoogleFormHistoryCreateRequest request,
            @RequestAttribute UserEntity user
    ) {
        return new ResponseEntity<>(service.create(request, user), HttpStatus.OK);
    }
}
