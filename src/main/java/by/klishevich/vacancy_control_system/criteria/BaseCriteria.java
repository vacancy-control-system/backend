package by.klishevich.vacancy_control_system.criteria;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Data;

/**
 * Superclass that implements a template method pattern for filtering entities
 * result
 * <a href=
 * "https://refactoring.guru/ru/design-patterns/template-method/java/example#lang-features">more
 * info about pattern</a>
 *
 * @param <T> - entity class
 */
@Data
public abstract class BaseCriteria<T> {
    private Optional<String> sortBy = Optional.empty();
    private Sort.Direction sortDirection = Sort.Direction.ASC;
    private Integer pageNumber = 1;
    private Integer pageSize = 25;
    private Optional<List<Long>> ids = Optional.empty();

    public List<Predicate> toPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new LinkedList<>();
        ids.ifPresent(ids -> predicates.add(root.get("id").in(ids)));

        return predicates;
    }

    public Pageable toPageable() {
        Sort sort = Sort.unsorted();
        sortBy.ifPresent(value -> Sort.by(sortDirection, value));

        return PageRequest.of(pageNumber - 1, pageSize, sort);
    }
}
