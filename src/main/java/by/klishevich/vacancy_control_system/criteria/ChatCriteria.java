package by.klishevich.vacancy_control_system.criteria;

import java.util.List;
import java.util.Optional;

import by.klishevich.vacancy_control_system.entity.chat.ChatEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Data;

@Data
public class ChatCriteria extends BaseCriteria<ChatEntity> {
    private Optional<Long> vacancyId = Optional.empty();

    @Override
    public List<Predicate> toPredicates(Root<ChatEntity> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.toPredicates(root, query, criteriaBuilder);
        vacancyId.ifPresent(id -> predicates.add(criteriaBuilder.equal(root.get("vacancy").get("id"), id)));

        return predicates;
    }
}
