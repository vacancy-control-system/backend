package by.klishevich.vacancy_control_system.criteria;

import java.util.LinkedList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;



public class CriteriaExtension<T> extends BaseCriteria<T>{
    private BaseCriteria<T> criteria;
    private List<PredicateBuilder<T>> extensions = new LinkedList<>();

    public CriteriaExtension(BaseCriteria<T> criteria) {
        this.criteria = criteria;
    }

    public void addPredicate(PredicateBuilder<T> builder) {
        extensions.add(builder);
    }

    public List<Predicate> toPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new LinkedList<>(criteria.toPredicates(root, query, criteriaBuilder));

        for (PredicateBuilder<T> extension : extensions) {
            predicates.add(extension.toPredicate(root, query, criteriaBuilder));
        }

        return predicates;
    }

    public interface PredicateBuilder<T> {
        Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);
    }
}
