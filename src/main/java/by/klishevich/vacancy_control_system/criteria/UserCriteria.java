package by.klishevich.vacancy_control_system.criteria;

import java.util.List;
import java.util.Optional;

import by.klishevich.vacancy_control_system.entity.user.RolesEnum;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Data;

@Data
public class UserCriteria extends BaseCriteria<UserEntity> {
    private Optional<RolesEnum> role = Optional.empty();

    @Override
    public List<Predicate> toPredicates(Root<UserEntity> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.toPredicates(root, query, criteriaBuilder);
        role.ifPresent(value -> predicates.add(criteriaBuilder.equal(root.get("role"), value)));

        return predicates;
    }
}
