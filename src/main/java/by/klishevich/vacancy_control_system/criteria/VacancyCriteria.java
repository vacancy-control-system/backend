package by.klishevich.vacancy_control_system.criteria;

import java.util.ArrayList;
import java.util.List;

import by.klishevich.vacancy_control_system.entity.vacancy.VacancyEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class VacancyCriteria extends BaseCriteria<VacancyEntity> {
    private List<Long> schedulesIdIn = new ArrayList<>();
    private List<Long> typeEmploymentIdIn = new ArrayList<>();

    @Override
    public List<Predicate> toPredicates(Root<VacancyEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.toPredicates(root, query, criteriaBuilder);
        if (!schedulesIdIn.isEmpty()){
            CriteriaBuilder.In<Long> in = criteriaBuilder.in(root.get("schedule").get("id"));
            schedulesIdIn.forEach(id -> in.value(id));
            predicates.add(in);
        }
        if (!typeEmploymentIdIn.isEmpty()){
            CriteriaBuilder.In<Long> in = criteriaBuilder.in(root.get("typeEmploymentEntity").get("id"));
            typeEmploymentIdIn.forEach(id -> in.value(id));
            predicates.add(in);
        }

        return predicates;
    }
}
