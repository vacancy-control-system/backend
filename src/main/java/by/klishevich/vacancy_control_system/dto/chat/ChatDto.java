package by.klishevich.vacancy_control_system.dto.chat;

import lombok.Data;

@Data
public class ChatDto {
    private Long id;
    private Long vacancyId;
    private Long userId;
}
