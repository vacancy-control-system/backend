package by.klishevich.vacancy_control_system.dto.chat;

import lombok.Data;

@Data
public class MessageDto {
    private String text;
    private String username;
    private Long userId;
}
