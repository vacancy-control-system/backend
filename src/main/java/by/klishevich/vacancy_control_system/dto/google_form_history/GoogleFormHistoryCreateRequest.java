package by.klishevich.vacancy_control_system.dto.google_form_history;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GoogleFormHistoryCreateRequest {
    private Long vacancyId;
}
