package by.klishevich.vacancy_control_system.dto.google_form_history;

import java.time.Instant;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GoogleFormHistoryDto {
    private Long id;
    private Long userId;
    private Instant date;
    private Long vacancyId;
}
