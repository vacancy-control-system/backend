package by.klishevich.vacancy_control_system.entity.vacancy;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import by.klishevich.vacancy_control_system.entity.BaseEntity;
import by.klishevich.vacancy_control_system.entity.Language;
import by.klishevich.vacancy_control_system.entity.chat.ChatEntity;
import by.klishevich.vacancy_control_system.entity.schedule.ScheduleEntity;
import by.klishevich.vacancy_control_system.entity.type_employment.TypeEmploymentEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "vacancy")
@NoArgsConstructor
public class VacancyEntity implements BaseEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "vacancy_id")
    @MapKeyColumn(name = "language")
    private Map<Language, LocalizedVacancyEntity> localizedData = new HashMap<>();

    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private ScheduleEntity schedule;

    @ManyToOne
    @JoinColumn(name = "type_employment_id")
    private TypeEmploymentEntity typeEmploymentEntity;

    @Column(name = "salary")
    private BigDecimal salary;

    @Column(name = "googleForm")
    private String googleForm;
}
