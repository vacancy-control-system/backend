package by.klishevich.vacancy_control_system.repository;

import java.util.Optional;

import by.klishevich.vacancy_control_system.entity.chat.ChatEntity;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.entity.vacancy.VacancyEntity;

public interface ChatRepository extends CrudRepository<ChatEntity, Long>{
    Optional<ChatEntity> findByUserAndVacancy(UserEntity user, VacancyEntity vacancy);

    Optional<ChatEntity> findByIdAndUser(Long id, UserEntity user);
}
