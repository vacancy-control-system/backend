package by.klishevich.vacancy_control_system.repository;

import by.klishevich.vacancy_control_system.entity.google_form_history.GoogleFormHistoryEntity;

public interface GoogleFormHistoryRepository extends CrudRepository<GoogleFormHistoryEntity, Long>{
    
}
