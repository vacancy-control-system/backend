package by.klishevich.vacancy_control_system.repository;

import java.util.List;

import by.klishevich.vacancy_control_system.entity.chat.ChatEntity;
import by.klishevich.vacancy_control_system.entity.chat.MessageEntity;

public interface MessageRepository extends CrudRepository<MessageEntity, Long>{
    List<MessageEntity> findAllByChat(ChatEntity chat);
}
