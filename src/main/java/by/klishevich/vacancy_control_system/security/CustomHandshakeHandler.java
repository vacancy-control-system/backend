package by.klishevich.vacancy_control_system.security;

import java.security.Principal;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class CustomHandshakeHandler extends DefaultHandshakeHandler {
    @Value("${jwt.token.secret}")
    private String SECRET;

    private final AuthService authService;

    @Override
    protected Principal determineUser(ServerHttpRequest request,
            WebSocketHandler wsHandler,
            Map<String, Object> attributes) {

        log.info(request.getHeaders().keySet().toString());
        log.info(attributes.toString());

        if (request.getHeaders().get("Authorization") == null
                || request.getHeaders().get("Authorization").isEmpty()) {
            return new CustomAuthentication(false, UserEntity.guest());
        }

        String bearerToken = request.getHeaders().get("Authorization").get(0);

        if (!bearerToken.startsWith("Bearer ")) {
            return new CustomAuthentication(false, UserEntity.guest());
        }

        String token = bearerToken.substring(7);

        Optional<UserEntity> user = authService.resolveUser(token);

        if (user.isPresent()) {
            return new CustomAuthentication(true, user.get());
        }

        return new CustomAuthentication(false, UserEntity.guest());
    }
}
