package by.klishevich.vacancy_control_system.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;

import by.klishevich.vacancy_control_system.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class WebSocketAuthInterceptor implements ChannelInterceptor {
    private final AuthService authService;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        final var accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        final var cmd = accessor.getCommand();

        if (!(StompCommand.CONNECT == cmd || StompCommand.SEND == cmd)) {
            return message;
        }

        final String requestTokenHeader = accessor.getFirstNativeHeader("Authorization");

        if (requestTokenHeader == null || !requestTokenHeader.startsWith("Bearer")) {
            return message;
        }

        String token = requestTokenHeader.substring(7);

        try {
            authService.resolveUser(token).ifPresent(user -> {
                accessor.setUser(new CustomAuthentication(true, user));

                Map<String, Object> attributes = new HashMap<>();
                attributes.put("user", user);
                
                accessor.setSessionAttributes(attributes);
            });
        }
        catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return message;
    }

}
