package by.klishevich.vacancy_control_system.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import by.klishevich.vacancy_control_system.criteria.BaseCriteria;
import by.klishevich.vacancy_control_system.criteria.CriteriaExtension;
import by.klishevich.vacancy_control_system.dto.chat.ChatDto;
import by.klishevich.vacancy_control_system.dto.chat.MessageDto;
import by.klishevich.vacancy_control_system.entity.PageDto;
import by.klishevich.vacancy_control_system.entity.chat.ChatEntity;
import by.klishevich.vacancy_control_system.entity.chat.MessageEntity;
import by.klishevich.vacancy_control_system.entity.user.RolesEnum;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.exceptions.NotFoundException;
import by.klishevich.vacancy_control_system.repository.ChatRepository;
import by.klishevich.vacancy_control_system.repository.MessageRepository;
import by.klishevich.vacancy_control_system.specification.SpecificationAdapter;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository repository;
    private final MessageRepository messageRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public void createMessage(Long id, UserEntity user, String text) {
        ChatEntity chat = repository.findById(id).orElseThrow(() -> new NotFoundException());

        if (user.getRole() == RolesEnum.ROLE_USER && !chat.getUser().getId().equals(user.getId())) {
            throw new NotFoundException();
        }

        MessageEntity message = new MessageEntity();
        message.setUser(user);
        message.setText(text);
        message.setChat(chat);

        messageRepository.save(message);

        String des = String.format("/events/chat/%s/new-message", id);

        log.warn(des);

        simpMessagingTemplate.convertAndSend(des, toDto(message));
    }

    public List<MessageDto> findMessages(Long id, UserEntity user) {
        ChatEntity entity = repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found"));

        if (user.getRole() == RolesEnum.ROLE_USER && !user.getId().equals(entity.getUser().getId())) {
            throw new NotFoundException("Not found");
        }
        else if (!(user.getRole() == RolesEnum.ROLE_INTERVIEWER || user.getRole() == RolesEnum.ROLE_USER)) {
            throw new NotFoundException("Not found");
        }

        return messageRepository.findAllByChat(entity).stream().map(this::toDto).collect(Collectors.toList());
    }

    public MessageDto toDto(MessageEntity entity) {
        MessageDto dto = new MessageDto();
        dto.setUserId(entity.getUser().getId());
        dto.setUsername(String.format("%s %s", entity.getUser().getName(), entity.getUser().getSurname()));
        dto.setText(entity.getText());

        return dto;
    }

    public PageDto<ChatDto> findAllPageable(BaseCriteria<ChatEntity> criteria) {
        final Page<ChatEntity> entities = repository.findAll(new SpecificationAdapter<>(criteria),
                criteria.toPageable());
        PageDto<ChatDto> pageDto = new PageDto<>();
        pageDto.setTotal(entities.getNumberOfElements());
        pageDto.setData(entities.map(this::toDto).toList());

        return pageDto;
    }

    public PageDto<ChatDto> findAllPageable(BaseCriteria<ChatEntity> criteria, UserEntity user) {
        CriteriaExtension<ChatEntity> criteriaExtension = new CriteriaExtension<>(criteria);

        criteriaExtension
                .addPredicate((Root<ChatEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
                    return criteriaBuilder.equal(root.get("user").get("id"), user.getId());
                });

        final Page<ChatEntity> entities = repository.findAll(new SpecificationAdapter<>(criteriaExtension),
                criteria.toPageable());
        PageDto<ChatDto> pageDto = new PageDto<>();
        pageDto.setTotal(entities.getNumberOfElements());
        pageDto.setData(entities.map(this::toDto).toList());

        return pageDto;
    }

    public List<ChatDto> findAll(BaseCriteria<ChatEntity> criteria) {
        List<ChatEntity> entities = repository.findAll(new SpecificationAdapter<>(criteria));

        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<ChatDto> findAll(BaseCriteria<ChatEntity> criteria, UserEntity user) {
        CriteriaExtension<ChatEntity> criteriaExtension = new CriteriaExtension<>(criteria);

        criteriaExtension
                .addPredicate((Root<ChatEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
                    return criteriaBuilder.equal(root.get("user").get("id"), user.getId());
                });

        List<ChatEntity> entities = repository.findAll(new SpecificationAdapter<>(criteriaExtension));

        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public ChatDto findById(Long id) {
        final ChatEntity entity = repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found"));

        return toDto(entity);
    }

    public ChatDto findById(Long id, UserEntity user) {
        final ChatEntity entity = repository.findByIdAndUser(id, user)
                .orElseThrow(() -> new NotFoundException("Not found"));

        return toDto(entity);
    }

    public ChatDto toDto(ChatEntity entity) {
        ChatDto dto = new ChatDto();
        dto.setId(entity.getId());
        dto.setUserId(entity.getUser().getId());
        dto.setVacancyId(entity.getVacancy().getId());

        return dto;
    }

}
