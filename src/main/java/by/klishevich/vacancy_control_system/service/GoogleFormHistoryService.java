package by.klishevich.vacancy_control_system.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import by.klishevich.vacancy_control_system.criteria.BaseCriteria;
import by.klishevich.vacancy_control_system.dto.google_form_history.GoogleFormHistoryCreateRequest;
import by.klishevich.vacancy_control_system.dto.google_form_history.GoogleFormHistoryDto;
import by.klishevich.vacancy_control_system.entity.PageDto;
import by.klishevich.vacancy_control_system.entity.google_form_history.GoogleFormHistoryEntity;
import by.klishevich.vacancy_control_system.entity.user.UserEntity;
import by.klishevich.vacancy_control_system.exceptions.BadRequestException;
import by.klishevich.vacancy_control_system.exceptions.NotFoundException;
import by.klishevich.vacancy_control_system.repository.GoogleFormHistoryRepository;
import by.klishevich.vacancy_control_system.repository.VacancyRepository;
import by.klishevich.vacancy_control_system.specification.SpecificationAdapter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class GoogleFormHistoryService {
    private final GoogleFormHistoryRepository repository;
    private final VacancyRepository vacancyRepository;

    public GoogleFormHistoryDto create(GoogleFormHistoryCreateRequest request, UserEntity user) {
        GoogleFormHistoryEntity entity = new GoogleFormHistoryEntity();
        entity.setUser(user);
        entity.setVacancy(vacancyRepository.findById(request.getVacancyId())
                .orElseThrow(() -> new BadRequestException("Invalid vacancy id")));

        repository.save(entity);

        return toDto(entity);
    }

    public PageDto<GoogleFormHistoryDto> findAllPageable(BaseCriteria<GoogleFormHistoryEntity> criteria) {
        final Page<GoogleFormHistoryEntity> entities = repository.findAll(new SpecificationAdapter<>(criteria),
                criteria.toPageable());
        PageDto<GoogleFormHistoryDto> pageDto = new PageDto<>();
        pageDto.setTotal(entities.getNumberOfElements());
        pageDto.setData(entities.map(this::toDto).toList());

        return pageDto;
    }

    public List<GoogleFormHistoryDto> findAll(BaseCriteria<GoogleFormHistoryEntity> criteria) {
        List<GoogleFormHistoryEntity> entities = repository.findAll(new SpecificationAdapter<>(criteria));

        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public GoogleFormHistoryDto findById(Long id) {
        final GoogleFormHistoryEntity entity = repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found"));

        return toDto(entity);
    }

    public GoogleFormHistoryDto toDto(GoogleFormHistoryEntity entity) {
        GoogleFormHistoryDto dto = new GoogleFormHistoryDto();
        dto.setId(entity.getId());
        dto.setUserId(entity.getUser().getId());
        dto.setVacancyId(entity.getVacancy().getId());
        dto.setDate(entity.getDate());

        return dto;
    }
}
